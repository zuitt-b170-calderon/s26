
const http = require("http");

const port = 3000;

const server = http.createServer((request, response)=>{
	if (request.url === "/greeting") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Hello User");
	}
	else if 
		(request.url === "/login") {
			response.writeHead(200, {"Content-Type": "text/plain"});
			response.end("Welcome to LogIn Page")
	}
	
	
	else{
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("Page not found");
	}
});
	
server.listen(port);

console.log('Server now running at localhost: ${port}');
